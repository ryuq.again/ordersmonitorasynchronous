# OrdersMonitorAsync ✓
Monitor wyświetlający produkty z określonej kategorii, wraz z numerami faktur.

Do działania, monitor potrzebuje tokenu użytkownika umieszczonego w pliku token.json w głównym folderze w takim formacie: 

```json
{
    "token": "<TOKEN>"
  }  
```
---

Nalezy najpierw uruchomic http-server w folderze ``public`` potem uruchomić ``start.js`` lub ``start.min.js``


